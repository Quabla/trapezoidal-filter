# script for packaging the vhdl/verilog hdl files into an "IP-Core" for the Xilinx toolchain
# this script is assuming a core name along the lines of "my_core_name_v1_0.v" where the "v1_0"
# part describes the version of the core.

# get the name of the core as argument (remember in tcl everything is a command, and thus can have arguments)
set core_name [lindex $argv 0]

# get the part name (no clue here what that means)
set part_name [lindex $argv 1]

# get the actual name (without the trailing version declaration)
set elements [split $core_name _]
set project_name [join [lrange $elements 0 end-2] _]
# get the version of the core
set version [string trimleft [join [lrange $elements end-1 end] .] v]

# make sure we are working with only the sources and not the remnants of a past run
file delete -force tmp/cores/$core_name tmp/cores/$project_name.cache tmp/cores/$project_name.hw tmp/cores/$project_name.xpr

create_project -part $part_name $project_name tmp/cores

add_files -norecurse [glob cores/$core_name/*.v]

# start packaging the core into the "IP" format of xilinx
ipx::package_project -import_files -root_dir tmp/cores/$core_name

set core [ipx::current_core]

set_property VERSION $version $core
set_property NAME $project_name $core
set_property LIBRARY {user} $core
set_property VENDOR {pavel-demin} $core
set_property VENDOR_DISPLAY_NAME {Pavel Demin} $core
set_property COMPANY_URL {https://github.com/pavel-demin/red-pitaya-notes} $core
set_property SUPPORTED_FAMILIES {zynq Production} $core

# a small helper for defining the display name and description in the gui
proc core_parameter {name display_name description} {
  set core [ipx::current_core]

  set parameter [ipx::get_user_parameters $name -of_objects $core]
  set_property DISPLAY_NAME $display_name $parameter
  set_property DESCRIPTION $description $parameter

  set parameter [ipgui::get_guiparamspec -name $name -component $core]
  set_property DISPLAY_NAME $display_name $parameter
  set_property TOOLTIP $description $parameter
}

# load the configuration parameters from the config script of the core (the core_config.tcl
# file describes the different properties of the core in detail
source cores/$core_name/core_config.tcl

rename core_parameter {}

# tell Vivado to create a gui representation and save the core in the "IP" format
ipx::create_xgui_files $core
ipx::update_checksums $core
ipx::save_core $core

close_project
