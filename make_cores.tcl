# ==================================================================================================
# make_cores.tcl
# by Anton Potocnik, 01.10.2016
# modified by Alexander Becker, 20.12.2020
# ==================================================================================================

set part_name xc7z010clg400-1

if {! [file exists cores]} {
	puts "Failed !";
	puts "The cores directory could not be found in this folder";
	return
} 

# generate a list of ip cores by looking into the folder
cd cores
set core_names [glob -type d *]
cd ..

# generate cores
foreach core $core_names {
	set argv "$core $part_name"
	puts "Generating $core";
	puts "===========================";
	source scripts/core.tcl
}
