# Trapezoidal-filter

A trapezoidal filter written in VHDL to be implemented in the PL of a Xilinx ZYNQ in a Red Pitaya board.

The Filter is a trapezoidal filter designed to record the time and height of a Pulse with exponential decay.
It is implemented in VHDL and scripts are included to be able to synthesize the Block together with all other needed
components on the ZYNQ of a RedPitaya. Many of the scripts are adaptations from Pavel Demins excellent red-pitaya-notes.
